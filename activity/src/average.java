
import java.util.Scanner;
public class average {
    public static void main(String[] args) {
        Scanner inputUser = new Scanner(System.in);

        System.out.println("First Name");
        String firstName = inputUser.nextLine();

        System.out.println("Last Name");
        String lastName = inputUser.nextLine();

        System.out.println("First Subject Grade:");
        double firstGrade =  Double.parseDouble(inputUser.nextLine());
        System.out.println("Second Subject Grade:");
        double secondGrade = Double.parseDouble(inputUser.nextLine());
        System.out.println("Third Subject Grade:");
        double thirdGrade = Double.parseDouble(inputUser.nextLine());

        double average;
        average = (firstGrade + secondGrade + thirdGrade )/3;

        System.out.println("Good day, "+ firstName +" "+ lastName);
        System.out.println("Your Grade Average is: "+ average);
    }

}
