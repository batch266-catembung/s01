package com.zuitt.example;
import java.util.Scanner; //  importing scanner class to handle user input
public class UserInput {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter Username");
        String userName = myObj.nextLine();
        System.out.println("Username is " + userName);
    }
}
