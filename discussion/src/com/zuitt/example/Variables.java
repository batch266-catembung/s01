package com.zuitt.example;

public class Variables {
    /*
    * primitive
    * int for integer
    * double for float
    * char for single character
    * boolean for boolean values
    * non-primitive
    * string
    * array
    * classes
    * interface
    * */
    public static void main(String[] args) {
        int age = 22;
        char middle_initial = 'V';
        boolean isLegalAge = true;
        System.out.println("the user age is " + age);
        System.out.println("the user middle initial is " + middle_initial);
        System.out.println("the user age is " + isLegalAge);
    }


}
